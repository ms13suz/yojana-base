<?php

use Carbon\Carbon as Carbon;
use Carbon\CarbonPeriod;
use Carbon\CarbonInterval as CarbonInterval;
use DateTime as DateTime;
use Yojana\Helpers\DateProvider;

if (!function_exists('printR')) {

    function printR($array,$toArray = false)
    {
        echo "<pre>";
        if($toArray === true) print_r($array->toArray());
        else print_r($array);
        die();
    }
}

if (!function_exists('isDatePast')) {
    //GET::CUURENT DATE 

    function isDatePast($date)
    {
        return Carbon::createFromFormat('Y-m-d H:s:i', $date)->isPast();
    }
}

if (!function_exists('currentDate')) {
    //GET::CUURENT DATE 

    function currentDate()
    {
        $now = Carbon::now();
        return $now;
    }
}

if (!function_exists('yesterday')) {
    //GET::YESTERDAY DATE 

    function yesterday()
    {
        $yesterday = Carbon::yesterday();
        return $yesterday;
    }
}

if (!function_exists('tomorrow')) {
    //GET::TOMORROW DATE 

    function tomorrow()
    {
        $tomorrow = Carbon::tomorrow();
        return $tomorrow;
    }
}

if (!function_exists('startOfWeek')) {
    //GET::DATE OF WEEK START 

    function startOfWeek()
    {
        $startOfWeek = currentDate()->startOfWeek();
        return $startOfWeek;
    }
}

if (!function_exists('endOfWeek')) {
    //GET::DATE OF WEEK END 

    function endOfWeek()
    {
        $endOfWeek = currentDate()->endOfWeek();
        return $endOfWeek;
    }
}

if (!function_exists('startOfLastWeek')) {
    //GET::DATE OF LAST WEEK START 

    function startOfLastWeek()
    {
        $startOfLastWeek =startOfWeek()->subWeek();
        return $startOfLastWeek;
    }
}

if (!function_exists('endOfLastWeek')) {
    //GET::DATE OF LAST WEEK END 

    function endOfLastWeek()
    {
        $endOfLastWeek = startOfLastWeek()->addDays(7);
        return $endOfLastWeek;
    }
}

if (!function_exists('startOfNextWeek')) {
    //GET::DATE OF NEXT WEEK START 

    function startOfNextWeek()
    {
        $startOfNextWeek = startOfWeek()->addWeeks(1);
        return $startOfNextWeek;
    }
}

if (!function_exists('endOfNextWeek')) {
    //GET::DATE OF NEXT WEEK END 

    function endOfNextWeek()
    {
        $endOfNextWeek = startOfNextWeek()->addDays(7);
        return $endOfNextWeek;
    }
}

if (!function_exists('createDate')) {
    //GET::DATE OF NEXT WEEK END 

    function createDate($year, $month, $day = 1, $tz = null)
    {
        return Carbon::createFromDate($year, $month, $day, $tz);
    }
}

if (!function_exists('diffInDate')) {
    //GET::DATE DIFFERENCE
    //INCLUDING START DATE ALSO
    function diffInDate($to,$from,$include = false)
    {

        $date_to = Carbon::createFromFormat('Y-m-d H:s:i', dateFormat($to,'Y-m-d H:s:i'));
        
        $date_from = Carbon::createFromFormat('Y-m-d H:s:i', dateFormat($from,'Y-m-d H:s:i'));

        $diff_in_days = $date_to->diffInDays($date_from);

        if($include) 
            return $diff_in_days + 1;
        else 
            return $diff_in_days;
    }
}


if (!function_exists('getNepaliDate')) {
    //GET::ENGLISH TO NEPALI DATE CONVERTER

    function getNepaliDate($date,$seperator = '-')
    {
        try {
            $date=dateFormat($date,'Y/m/d'); 
          
            $query = \DB::table('date_conversion')->where('ad_date', $date)
                ->select('bs_date')
                ->first();
            
           
            $result = str_replace('/',$seperator,$query->bs_date);
           
            return $result;
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}

if (!function_exists('getEnglishDate')) {
    //GET::NEPALI TO ENGLISH DATE CONVERTER

    function getEnglishDate($date,$seperator = '-')
    {
        try {
            if (strpos((string)$date, '-') !== false) {
                $date = str_replace('-','/',$date);
            }

            $query = \DB::table('date_conversion')
                ->where('bs_date', $date)
                ->select('ad_date')
                ->first();
            $result = str_replace('/',$seperator,$query->ad_date);
            return $result;
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}

if (!function_exists('getNepaliMonthLastDate')) {
    //GET::LAST DATE OF MONTH

    function getNepaliMonthLastDate($year,$month,$seperator)
    {
        try {
            $query = \DB::table('date_conversion')
                ->whereYear('bs_date', $year)
                ->whereMonth('bs_date', $month)
                ->orderBy('bs_date','DESC')
                ->select('bs_date')
                ->first();

            $result = str_replace('/',$seperator,$query->bs_date);
            return $result;
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}

if (!function_exists('getNepaliMonthlyDate')) {
    //GET::NEPALI DATE ARRAY
    function getNepaliMonthlyDate($year,$month)
    {
        try {
            $year = (int)$year;
            $month = (int)$month;
            $dateArray = [];
            $count = 0;
            $neplaiDate = new DateProvider();
            foreach($neplaiDate->nepaliDates as $yearData) {
                
                if($yearData[0] == $year) {
                    for ($i = $month; $i <= 12; $i++) {
                        if(strlen((string)abs($i)) === 1)
                        {
                            $start_date_bs = $year .'-0'.$i.'-01';
                            $end_date_bs = $year .'-0'.$i.'-'.$yearData[$i];
                        }else{
                            $start_date_bs = $year .'-'.$i.'-01';
                            $end_date_bs = $year .'-'.$i.'-'.$yearData[$i];
                        }
                        $dateArray[]= [
                            'start_date_bs' => $start_date_bs,
                            'end_date_bs' => $end_date_bs,
                            'start_date_ad' => getEnglishDate($start_date_bs),
                            'end_date_ad' => getEnglishDate($end_date_bs),
                        ];
                        $count ++;
                    }
                }
    
                if($yearData[0] == ($year + 1)) {
                    if($count <= 12){
                        $remaining = 12 - $count;
                        $next_year = $year + 1;
                        if($yearData[0] == $next_year) {
                            for ($i = 1; $i <= $remaining; $i++) {
                                if(strlen((string)abs($i)) === 1)
                                {
                                    $start_date_bs = $next_year .'-0'.$i.'-01';
                                    $end_date_bs = $next_year .'-0'.$i.'-'.$yearData[$i];
                                }else{
                                    $start_date_bs = $next_year .'-'.$i.'-01';
                                    $end_date_bs = $next_year .'-'.$i.'-'.$yearData[$i];
                                }
                                $dateArray[]= [
                                    'start_date_bs' => $start_date_bs,
                                    'end_date_bs' => $end_date_bs,
                                    'start_date_ad' => getEnglishDate($start_date_bs),
                                    'end_date_ad' => getEnglishDate($end_date_bs),
                                ];
                                $count ++;
                            }
                        }
                    }
                }
            }
    
            return $dateArray;
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}



if (!function_exists('subtractDay')) {

    function subtractDay($date,$day = 0)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->subDays($day);
    }
}

if (!function_exists('dateFormat')) {

    function dateFormat($date,$format)
    {
        return Carbon::parse($date)->format($format);
    }
}

if (!function_exists('overlapInMinutes')) {
function overlapInMinutes($startDate1, $endDate1, $startDate2, $endDate2)
    {
        // Figure out which is the later start time
        $lastStart = $startDate1 >= $startDate2 ? $startDate1 : $startDate2;
        // Convert that to an integer
        $lastStart = strtotime($lastStart);

        // Figure out which is the earlier end time
        $firstEnd = $endDate1 <= $endDate2 ? $endDate1 : $endDate2;
        // Convert that to an integer
        $firstEnd = strtotime($firstEnd);

        // Subtract the two, divide by 60 to convert seconds to minutes, and round down
        $overlap = floor( ($firstEnd - $lastStart) / 60 );

        // If the answer is greater than 0 use it.
        // If not, there is no overlap.
        return $overlap > 0 ? $overlap : 0;
    }
}

if (!function_exists('overlapInDate')) {
    function overlapInDate($startDate1, $endDate1, $startDate2, $endDate2)
        {
            // Figure out which is the later start time
            $lastStart = $startDate1 >= $startDate2 ? $startDate1 : $startDate2;
            // Convert that to an integer
            $lastStart = strtotime($lastStart);
    
            // Figure out which is the earlier end time
            $firstEnd = $endDate1 <= $endDate2 ? $endDate1 : $endDate2;
            // Convert that to an integer
            $firstEnd = strtotime($firstEnd);
    
            // Subtract the two, divide by 60 to convert seconds to minutes, and round down
            $overlap = floor( ($firstEnd - $lastStart) / 60 / 60 );
    
            // If the answer is greater than 0 use it.
            // If not, there is no overlap.
            return $overlap > 0 ? $overlap : 0;
        }
}

if (!function_exists('sortDate')) {

    function sortDate($from,$to)
    {
        return strtotime($from) - strtotime($to);
    }
}

if (!function_exists('checkContinious')) {
    //GET::DATE OF LAST WEEK END 

    function checkContinious($start_date,$next_start_day)
    {
        $startDate=dateFormat(Carbon::parse($start_date)->addDays(1),'Y-m-d');
        $endDate=dateFormat($next_start_day,'Y-m-d');
        if($startDate === $endDate)
            return $startDate;
        else 
            return NULl;
    }
}

if (!function_exists('getEffectiveHours')) {
    //GET::Effective Hours in decimal

    function getEffectiveHours($start_date,$end_date)
    {
        $t1 = Carbon::parse($start_date);
        $t2 = Carbon::parse($end_date);
        $totalMin = $t1->diffInMinutes($t2);
        return $totalMin ? $totalMin/60 : null;
    }
}

if (!function_exists('getEffectiveDays')) {
    //GET::DATE OF LAST WEEK END 

    function getEffectiveDays($start_date,$end_date)
    {
        $t1 = Carbon::parse($start_date);
        $t2 = Carbon::parse($end_date);
        $totalMin = $t1->diffInHours($t2);
        return $totalMin ? $totalMin/24 : null;
    }
}

if (!function_exists('appendTimeToDate')) {
    //GET::DATE OF LAST WEEK END 

    function appendTimeToDate($date = null,$time)
    {
        $newDate = $date ? new DateTime($date):new DateTime();

        $newTime = new DateTime($time);
        $newDate->setTime($newTime->format('H'), $newTime->format('i'), $newTime->format('s'));

       return $newDate->format('Y-m-d H:i:s');
    }
}

if (!function_exists('isValidDateTime')) {
    // CHECK VALID DATE TIME
    function isValidDateTime($dateTime)
    {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1])) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('numberFormat')) {
    // CHECK VALID DATE TIME
    function numberFormat($number)
    {
        if($number) return number_format($number,2,'.','');
        
        return '0.00';
    }
}

if (!function_exists('keyExists')) {
    //CHECK OBJECT KEY EXISTS OR NOT

    function keyExists($array,$objectName)
    {
        // return (isset($array[$objectName]) || property_exists($array,$objectName));
        return (isset($array[$objectName]));
    }
}

if (!function_exists('nested_array_search')) {
    //SEACRH NESTED ARRAY
    function nested_array_search($array, $key, $value)
    {
        $results = array();

        if (is_array($array))
        {
            if (isset($array[$key]) && $array[$key] == $value)
                $results[] = $array;

            foreach ($array as $subarray)
                $results = array_merge($results, nested_array_search($subarray, $key, $value));
        }

        return $results;
    }
}

if (!function_exists('getMonthListFromDate')) {
    //GET ALL MONTH BETWEEN DATES
    function getMonthListFromDate($start,$end)
    {
        $startDate = new Carbon($start);
        $endDate = new Carbon($end);
        if(empty($end)) {
            $end = currentDate();
            $endDate = currentDate();
        }
        $i = 0; 
        $monthArray=CarbonPeriod::create($startDate->startOfMonth(), '1 month', $end);
        if($startDate->startOfMonth() == $endDate->startOfMonth()){
            $months=[dateFormat($start,'Y-m-d')=>dateFormat($end,'Y-m-d')];  
        }else{
            foreach ($monthArray as $month) {
                if ($i === 0){
                    $months[dateFormat($start,'Y-m-d')] = $month->endOfMonth()->format('Y-m-d');
                }else if( $i == count( $monthArray ) - 1){
                    if($endDate->startOfMonth()->format('Y-m-d') !== dateFormat($end,'Y-m-d')) 
                        $months[$month->startOfMonth()->format('Y-m-d')] = dateFormat($end,'Y-m-d');
                }else
                    $months[$month->format('Y-m-d')] = $month->endOfMonth()->format('Y-m-d');
    
                $i = $i + 1; 
            }
        }
        return $months;
    }
}

if (!function_exists('getDatesFromRange')) {
    function getDatesFromRange($date_time_from, $date_time_to)
    {
       
        $start = Carbon::createFromFormat('Y-m-d', substr($date_time_from, 0, 10));
        $end = Carbon::createFromFormat('Y-m-d', substr($date_time_to, 0, 10));

        $dates = [];

        while ($start->lte($end)) {

            $dates[] = $start->copy()->format('Y-m-d');

            $start->addDay();
        }

        return $dates;
    }
}

if (!function_exists('getDatesFromRangeWithTime')) {
    function getDatesFromRangeWithTime($date_time_from, $date_time_to)
    {
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $date_time_from);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $date_time_to);
        $i = 0; 
        $dates = [];
        if(dateFormat($date_time_from,'Y-m-d') === dateFormat($date_time_to,'Y-m-d')){
            $merge=[$date_time_from => $date_time_to] ;
        }else{
            while ($start->lte($end)) {
                if($i === 0 && dateFormat($date_time_from,'H:i:s') ==='13:00:00')
                    $dates[$start->copy()->format('Y-m-d H:i:s')] = $start->copy()->format('Y-m-d 18:00:00');
                else
                    $dates[$start->copy()->format('Y-m-d 09:00:00')] = $start->copy()->format('Y-m-d 18:00:00');
                $start->addDay();
                $i++;
            } 
            // array_shift($dates);
            array_pop($dates);
            $merge=array_merge($dates,[dateFormat($date_time_to,'Y-m-d 09:00:00') => $date_time_to]);
        }
        return $merge;
    }
}

if (!function_exists('getAllSpecificDayOfMonth')) {

    function getAllSpecificDayOfMonth($y,$m,$day)
    {
        return new \DatePeriod(
            Carbon::parse("first $day of $y-$m"),
            CarbonInterval::week(),
            Carbon::parse("next month $y-$m-01")
        );
    }
}

if (!function_exists('findDateOfDayInMonth')) {

    function findDateOfDayInMonth($y, $m,$day,$format='Y-m-d')
    {
        $dayDate = getAllSpecificDayOfMonth($y,$m,$day);
        $dateArray=[];
        foreach ($dayDate as $date) {
            $dateArray[]=[
                "type"=>"Weekend",
                "date"=>$date->format($format)
            ];
        }
        return $dateArray;
    }
}

if (!function_exists('daysInMonth')) {

    function daysInMonth($year, $month)
    {
        return Carbon::createFromDate($year,$month)->daysInMonth;
    }
}

if (!function_exists('startDateOfMonth')) {

    function startDateOfMonth($year, $month)
    {
        return Carbon::parse("first day of $year-$month");
    }
}

if (!function_exists('endDateOfMonth')) {

    function endDateOfMonth($year, $month)
    {
        return Carbon::parse("last day of $year-$month");
    }
}

if (!function_exists('dateDiffInterval')) {

    function dateDiffInterval($sDate1, $sDate2, $sUnit='H') {
        //subtract $sDate2-$sDate1 and return the difference in $sUnit (Days,Hours,Minutes,Seconds)
        $nInterval = strtotime($sDate2) - strtotime($sDate1);
        if ($sUnit=='D') { // days
            $nInterval = $nInterval/60/60/24;
        } else if ($sUnit=='H') { // hours
            $nInterval = $nInterval/60/60;
        } else if ($sUnit=='M') { // minutes
            $nInterval = $nInterval/60;
        } else if ($sUnit=='S') { // seconds
        }
        return $nInterval;
    }
}



if (!function_exists('getMonthName')) {
    //MONTH
    function getMonthName($monthId)
    {
        switch ($monthId) {
            case 1:
                return "January";
                break;
            case 2:
                return "February";
                break;
            case 3:
                return "March";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "May";
                break;
            case 6:
                return "June";
                break;
            case 7:
                return "July";
                break;
            case 8:
                return "August";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "October";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "December";
                break;
            default:
                return "Invalid";
        }
    }
}

if (!function_exists('getNepaliMonthName')) {
    //MONTH
    function getNepaliMonthName($monthId)
    {
        switch ($monthId) {
            case 1:
                return "वैशाख";
                break;
            case 2:
                return "जेठ";
                break;
            case 3:
                return "असार";
                break;
            case 4:
                return "साउन";
                break;
            case 5:
                return "भदौ";
                break;
            case 6:
                return "असोज";
                break;
            case 7:
                return "कार्तिक";
                break;
            case 8:
                return "मंसिर";
                break;
            case 9:
                return "पुष";
                break;
            case 10:
                return "माघ";
                break;
            case 11:
                return "फागुन";
                break;
            case 12:
                return "चैत";
                break;
            default:
                return "Invalid";
        }
    }
}

if (!function_exists('getDay')) {
    //DAY
    function getDay($day)
    {
        switch ($day) {
            case 1:
                return "Sunday";
                break;
            case 2:
                return "Monday";
                break;
            case 3:
                return "Tuesday";
                break;
            case 4:
                return "Wednesday";
                break;
            case 5:
                return "Thursday";
                break;
            case 6:
                return "Friday";
                break;
            case 7:
                return "Saturday";
                break;
            default:
                return "Invalid";
        }
    }
}

if (!function_exists('check_in_range')) {
    //TO CHECK IF DATE EXSIT BETWEEN RANGES
    function check_in_range($start_date, $end_date, $check_date)
    {
    // Convert to timestamp
    $start_ts = strtotime($start_date);
    $end_ts = strtotime($end_date);
    $user_ts = strtotime($check_date);

    // Check that user date is between start & end
    return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
}


if (!function_exists('mergePerKey')) {
    //MERGE WITH VALUE
    function mergePerKey($arr1)
    {
        $outer_array = array();
        $unique_array = array();
        foreach($arr1 as $key => $value)
        {
            $inner_array = array();
        
            $fid_value = $value['date'];
            if(!in_array($value['date'], $unique_array))
            {
                array_push($unique_array, $fid_value);
                // unset($value['date']);
                array_push($inner_array, $value);
                $outer_array[$fid_value] = $inner_array;
            }else{
                // unset($value['date']);
                array_push($outer_array[$fid_value], $value);
        
            }
        }
        ksort($outer_array);
        return $outer_array;
    }
}

if (!function_exists('sortArray')) {
    function sortArray($array,$field){
        array_multisort(array_map('strtotime',array_column($array,$field)),SORT_ASC,$array);
        return $array;
    }
}

if (!function_exists('groupBy')) {
    function groupBy($key,$subKey,$data){
        $result = [];

        foreach($data as $val){
            if(keyExists($key,$val)){
                $result[$val[$key][$subKey]][]=$val;
            }else{
                $result[""][]=$val;
            }
        }
        return $result;
    }
}

if (!function_exists('multiKeyExists')) {
    function multiKeyExists(array $arr, $key) {
        // is in base array?
        if (keyExists($key, $arr)) {
            return true;
        }

        // check arrays contained in this array
        foreach ($arr as $element) {
            if (is_array($element)) {
                if (multiKeyExists($element, $key)) {
                    return true;
                }
            }
            
        }

        return false;
    }
}

if (!function_exists('array_value_recursive')) {
    function array_value_recursive(array $arr , $key)
    {
        $val = array();
        array_walk_recursive($arr, function ($v, $k) use ($key, &$val) {
            if ($k == $key) array_push($val, $v);
        });

        $data = []; 
        if($val){
            foreach($val as $filter){
                if(!strtotime($filter)) $data[] = $filter;
            }
        }
        return array_values(array_unique($data));
    }
}

if (!function_exists('array_keys_exist')) {
    function array_keys_exist( array $array, $keys ) {
        $count = 0;
        if ( ! is_array( $keys ) ) {
            $keys = func_get_args();
            array_shift( $keys );
        }
        foreach ( $keys as $key ) {
            if ( isset( $array[$key] ) || keyExists( $key, $array ) ) {
                $count ++;
            }
        }

        return count( $keys ) === $count;
    }
}
if (!function_exists('encrypt_decrypt')) {
    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

if (!function_exists('sort_array_of_array')) {
    function sort_array_of_array(&$array, $subfield)
        {
            $sortarray = array();
            foreach ($array as $key => $row)
            {
                $sortarray[$key] = $row[$subfield];
            }

            array_multisort($sortarray, SORT_ASC, $array);

            return $array;
        }
    }
}

if (!function_exists('get_only_specific_field_array')) {
    function get_only_specific_field_array($array, $field)
    {
        $newArray = array();
        foreach ($array as $key => $row)
        {
            $newArray[]= $row[$field];
        }
        return $newArray;
    }
}



