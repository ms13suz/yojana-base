<?php

namespace Yojana\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Pearl\RequestValidate\RequestAbstract;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Yojana\Traits\CommonTrait;

class BaseFormRequest extends RequestAbstract
{
    use CommonTrait;
    
    public function __construct()
    {
        //GET CURRENT USER INFORMATION FROM COMMON TRAITS
        $this->getAuthUser();
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator):ValidationException
    {
        $response = $this->validation_response($validator->errors());
        throw new HttpResponseException(response()->json($response, $response['code']));
    }
}
