<?php

namespace Yojana\Http\Repositories;

use Illuminate\Database\Eloquent\Model;
use Yojana\Traits\{CommonTrait,FileUploadTrait,RepoTrait};

abstract class YojanaBaseRepository implements YojanaBaseInterface
{
    use CommonTrait,FileUploadTrait,RepoTrait;

    protected $uploadPath;
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;

        //GET CURRENT USER INFORMATION FROM COMMON TRAITS
        $this->getAuthUser();
    }

    public function findById($where = [])
    {
        $model = $this->model;
       
        $item = $model->where($where)->first();
       
        return $item;
    }

    public function find($where = [], $with = [])
    {
        $model = $this->model;
        
        if (!empty($with)) {
            return $model->with($with)->where($where)->first();
        }
        return $model->where($where)->first();
    }

    public function findAll($where = [], $with = [])
    {
        $model = $this->model;

        if (!empty($with)) 
            return $model->with($with)->where($where)->get();
        else 
            return $model->where($where)->get();
    }

    public function save($data)
    {
        $model = $this->model->newInstance();
        $model->fill($data);

        // dd($model);
        $model->save();

        // dd($model);
        return $model;
    }

    public function update($data = [], $where = [])
    {
        $model = $this->model;

        $id = $data['id'];

        unset($data['id']);

        if (!empty($where))
            $item = self::find($where);
        else
            $item = self::findById(['id'=>$id]);
     
        if (empty($item)) {
            self::error_404();
        }
       
        if (!empty($where)){
            $item = $model->where($where)->update($data);
            $result= $model->find($where);
        }else{
            $item = $model->find($id)->update($data);
            $result= $model->find($id);
        }

        return $result;
    }

    public function delete($where = [])
    {
        $model = $this->model;

        $item = $model->where($where);
        if (empty($item)) {
            $this->error_404();
        }
        $res= $model->find($where);
        $result = $model->where($where)->delete();
        return $res;
    }

    public function softDelete($where = [],$data = [])
    {
        $model = $this->model;

        $item = $model->where($where);
        if (empty($item)) {
            $this->error_404();
        }
        $result = $model->where($where)->update($data);
        $res= $model->find($where);
        return $res;
    }

    public function upload($data, array $valid_extensions, $max_size=null)
    {
        $d['file_content'] = $data['file_content'];
        $d['file_name'] = $data['file_name'];
        $directory_path = $data['directory_path'];

        return $this->uploadFileByTrait($directory_path, $d, $valid_extensions, $max_size);
    }

    public function unlinkFile($file_path)
    {
        if (file_exists($file_path)) {
            unlink($file_path);
            return true;
        }
        return false;
    }

    // WORK WITH MODEL PASSED IN FUNCTION
    public function findById_WM($where = [], $model)
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        $item = $model->where($where)->first();
        // dd($item);
        return $item;
    }

    public function find_WM($where = [], $with = [], $model)
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        if (!empty($with)) {
            return $model->with($with)->where($where)->first();
        }
        return $model->where($where)->first();
    }

    public function findAll_WM($where = [], $with = [], $model)
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        if (!empty($with))
            return $model->with($with)->where($where)->get();
        else 
            return $model->where($where)->get();
    }

    public function save_WM($data, $model)
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }

        $item = $model->create($data);
        return $item;
    }

    public function update_WM($data = [], $where = [],  $model)
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
       
        if (!empty($where)) 
            $item = self::findAll_WM($where,[],$model);
        else 
            $item = self::findById_WM(['id'=>$data['id']],$model);
            
        if (empty($item)) {
            self::error_404();
        }
       
        if (!empty($where))
            $item = $model->where($where)->update($data);
        else
            $item = $model->find($data['id'])->update($data);

        if (!empty($where)) 
            $result = self::findAll_WM($where,[],$model);
        else 
            $result = self::findById_WM(['id'=>$data['id']],$model);

        return $result;
    }

    public function delete_WM($where = [], $model)
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        $item = $model->where($where);
        if (empty($item)) {
            $this->error_404();
        }
        $res= $model->find($where);
        $result = $model->where($where)->delete();
        return $res;
    }

    //['LIKE,EQUAL,WHEREIN,WHERENULL,LESSTHAN,EQUAL,GREATERTHAN....' => 'Database column field name']
    //$arrayFilter = array(
    //     'code'=>['operator' => 'LIKE','alais'=>'search_text'],
    //     'name'=>['operator' => 'LIKE','alais'=>'search_text'],
    // );

    public function list_WM($model, $where = [],$with = [], $request = [], $arrayFilter=[])
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        //CHECK REQUEST PARAMTERS
        $page_size = keyExists($request,'pageSize')?$request['pageSize']:'';
        $page = keyExists($request,'page')?$request['page']:'';
        $sorted = keyExists($request,'sorted')?$request['sorted']:'';
        $filtered = keyExists($request,'filtered')?$request['filtered']:'';
        $custom_filtered = keyExists($request,'customFiltered')?$request['customFiltered']:'';

        $no_of_pages = '';
        if ($page == 0) {
            $start_page = 0;
        } else {
            $start_page = ($page * $page_size);
        }

        $query = $model->with($with)->where($where);

        $nquery = clone($query);

        if (!empty($start_page)) {
            $query->offset($start_page);
        }

        if ($page_size) {
            $query->limit($page_size);
        }
        
        if (!empty($filtered) && !empty($arrayFilter)) {
            foreach($arrayFilter as $key => $item){
                if(keyExists($filtered,$item['alais']) && !empty($filtered[$item['alais']])){
                    if($item['operator'] === 'LIKE'){
                        $query->where($key , 'like', "%" .$filtered[$item['alais']] . "%");
                        $nquery->where($key , 'like', "%" .$filtered[$item['alais']] . "%");
                    }else{
                        $query->where($key ,$item['operator'], $item['alais']);
                        $nquery->where($key ,$item['operator'], $item['alais']);
                    }
                }
            }
            
        }

        $data = $query->get();
        $i = 0;
        $all_filtered_data = $nquery->get();
        $count = count($all_filtered_data);
        if (!empty($page_size)) {
            $no_of_pages = ceil($count / $page_size);
        }

        return array('rows' => $data, 'pages' => $no_of_pages ,'count'=>$count);
    }

    public function softDelete_WM($model, $where = [],$data = [])
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        
        $item = $model->where($where);
        if (empty($item)) {
            $this->error_404();
        }
        
        $data['deleted_at'] = currentDate()->format('Y-m-d h:i:s');

        $result = $model->where($where)->update($data);

        $res= $model->find($where);
        
        return $res;
    }

    public function isDirty($model, $request, $column = [],$where = [])
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        if(count($where) > 0)
            $query = $model->where($where)->first();
        else
            $query = $model->find($request['id']);


        if (empty($query)) {
            $this->error_404();
        }

        if(!empty($column)){
            foreach($column as $key => $field){
                if(keyExists($request,$field)){
                    $query[$field] = $request[$field];
                }
            }
        }
        
        return $query->isDirty();
    }

    public function getArrayOfSpecificField($model, $column = 'id', $where = [])
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }

        $query = $model->where($where)->get();

        if (empty($query)) {
            $this->error_404();
        }

        $id_array = [];
        
        if(count($query) > 0){
            foreach($query as $item){
                $id_array[]= $item[$column];
            }
        }
        
        return $id_array;
    }


    public function getCurrentPaymentData($query,$payment_date)
    {
        $nquery = clone($query);

        $latestData = $query->first();

        if(!empty($latestData)){
            $latest_month = dateFormat($latestData->start_date_ad,'m');
            $latest_year = dateFormat($latestData->start_date_ad,'Y');
            $latest_day = dateFormat($latestData->start_date_ad,'d');

            $payment_month = dateFormat($payment_date,'m');
                
            $data = $nquery->get();

            $allPeriodData = $data ? $data->toArray() : [];
            if((int)$latest_month === (int)$payment_month){
                if((int)$latest_day > 1 && count($allPeriodData) > 0){
                    $array = [];
                    foreach($allPeriodData as $item){
                        $start_date_month = dateFormat($item['start_date_ad'],'m');
                        $end_date_month = $item['end_date_ad'] ? dateFormat($item['end_date_ad'],'m') : 0;
                        if(((int)$start_date_month == (int)$latest_month) || ((int)$end_date_month == (int)$latest_month)){
                            $array[] = $item;
                        }
                    }
                    return sortArray($array,'start_date_ad');
                }
            }

            return [$latestData->toArray()];
        }
        return false;
    }

    public function checkIfExistOrCreate($model, $field_name, $field_value, $data = [],$where = [])
    {
        if(!$model instanceof Model) {
            $model = app($model);
        }
        
        // $query = $model->whereRaw('LOWER(`'.$field_name.'`) LIKE ? ',[trim(strtolower($field_value)).'%'])
        //         ->first();
   
        $query = $model->where($where)->where($field_name,trim(strtolower($field_value)))->first();

        if($query){
            return $query;
        }else{
            $data[$field_name] = $field_value;
            return $model->create($data);
        }
    }


    public function autoSuggest($request,$field_value, $page = 0, $page_size = null)
    {
        $model = $request['model'];
        $alias_column = $request['alias_column'];
        $value = $request['value'];
        $field_name = $request['column'];
        $table = $request['table'];

        if(!$model instanceof Model) {
            $model = app($model);
        }
        
        $start_page = 0;

        if($page > 0) $start_page = ($page * $page_size);

        $query =$model->distinct();

        foreach($field_name as $column)
        {
            if($column['operator'] === 'LIKE')
                $query->orwhereRaw('LOWER(`'.$column['field'].'`) LIKE ? ',[trim(strtolower($field_value)).'%']);
            else if($column['operator'] === 'EQUAL')
                $query->orWhere($column['field'],$field_value);
        }

        // $query->select('id', \DB::raw("CONCAT(`$table`.`name`,'|',`$table`.`code`) as name"))
        //     ->orderBy('id');

        $query->select('id', \DB::raw("CONCAT_WS(' | ',$alias_column) as label"),\DB::raw("CONCAT($value) as value"))
            ->orderBy('id');
 
        if (!empty($start_page)) {
            $query->offset($start_page);
        }

        if ($page_size) {
            $query->limit($page_size);
        }

        return $query->get();
    }

    public function upload_WM($media, $file_content, $file_name,  $id)
    {
        $d['file_content'] = $file_content;
        $d['file_name'] = $file_name;
        $d['directory_path'] = $media['path'].$id;
        
        return $this->upload($d,$media['valid_extensions'], $media['max_size_bytes']);
    }


    public function userJsonResponse()
    {
        if($this->logged_user){
            return json_encode ([
                'full_name' => $this->logged_user->full_name,
                'email' => $this->logged_user->email,
                'phone' => $this->logged_user->phone,
                'account_id' => $this->logged_user->account_id,
                'account_user_uuid' => $this->logged_user->account_user_uuid,
            ]);
        }

        return null;
    }
}
