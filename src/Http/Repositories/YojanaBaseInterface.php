<?php

namespace Yojana\Http\Repositories;

interface YojanaBaseInterface
{
    public function findById($where=[]);

    public function find($where = [],$with=[]);

    public function findAll($where = [],$with=[]);

    public function save($data);

    public function update($data = [],$where = []);

    public function delete($where = []);

    // public function upload($blog_id, $file = []);

    public function findById_WM($where=[], $model);

    public function find_WM($where = [],$with=[], $model);

    public function findAll_WM($where = [],$with=[], $model);

    public function save_WM($data, $model);

    public function update_WM($data = [], $where = [], $model);

    public function delete_WM($where = [], $model);

    public function list_WM($model, $where = [],$with = [], $request = [], $arrayFilter=[]);
}
