<?php

namespace Yojana\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Response;
use Yojana\Traits\{CommonTrait,FileUploadTrait,RepoTrait};

class Controller extends BaseController
{
    use CommonTrait,FileUploadTrait,RepoTrait;
   /**
     * @OA\Info(
     *   title=SWAGGER_LUME_TITLE,
     *   description="RESTful API template made from lumen",
     *   version="1.0",
     *   @OA\Contact(
     *     email="lloricode@gmail.com",
     *     name="Lloric Mayuga Garcia"
     *   ),
     *      @OA\License(
     *          name="MIT",
     *          url="https://opensource.org/licenses/MIT"
     *      )
     * )
     * @OA\Server(
     *      url=SWAGGER_LUME_CONST_HOST,
     *      description="API Server"
     * )
     *  @OA\Response(
     *         response="200",
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="token_type",
     *                         type="string",
     *                         description="Bearer"
     *                     ),
     *                     @OA\Property(
     *                         property="expires_in",
     *                         type="integer",
     *                         description="Token expiration in miliseconds",
     *                         @OA\Items
     *                     ),
     *                     @OA\Property(
     *                         property="access_token",
     *                         type="string",
     *                         description="JWT access token"
     *                     ),
     *                     @OA\Property(
     *                         property="refresh_token",
     *                         type="string",
     *                         description="Token type"
     *                     ),
     *                     example={
     *                         "token_type": "bearer",
     *                         "expires_in": 3600,
     *                         "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJ...",
     *                         "refresh_token": "def50200b10ed22a1dab8bb0d18..."
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     * )
     *
     * @OA\Tag(
     *     name="Authorization",
     *     description="API Endpoints of Authorization"
     * )
     * @OA\Tag(
     *     name="Localizations",
     *     description="API Endpoints of Localizations"
     * )
     *
     * @OA\Schema(
     *      schema="Error",
     *      required={"message"},
     *      @OA\Property(
     *          property="message",
     *          type="string"
     *      )
     *  ),
     */

    /**
     * @OA\SecurityScheme(
     *     type="http",
     *     description="Login with email and password to get the authentication token",
     *     name="Token based Based",
     *     in="header",
     *     scheme="bearer",
     *     bearerFormat="JWT",
     *     securityScheme="apiAuth",
     * )
     */

    public function __construct()
    {
        //GET CURRENT USER INFORMATION FROM COMMON TRAITS
        $this->getAuthUser();
    }

    public function sendResponse($result, $message=null, $user = null,$warning = null)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message,
            'warning' => $warning,
            'code' => Response::HTTP_OK
        ];
        // $response['logged_user'] = $user;
        return response()->json($response, Response::HTTP_OK, $headers = [], JSON_PRETTY_PRINT);
    }

    public function sendError($error, $code = 500, $errorMessages = [],$warning = null)
    {
        $response = [
            'success' => false,
            'message' => $error,
            'code' => $code,
            'warning' => $warning,
        ];
        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }
        return response()->json($response,$code);
    }

    public function sendException(Throwable $e)
    {
        $code = $e->getCode();
        if($code == 23000){
            $code= 500;
            $response = [
                'success' => false,
                'message' => 'This item cannot be deleted. This is being referenced by some other item(s).'
            ];
        }else{
            if (empty($code)) $code = 500;
            else if (!is_numeric($code)) $code = 500;
            else if(!$this->isValidHttpStatusCode($code)) $code=500;
    
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    
        return response()->json($response, $code);
    }

    public function sendNotAuthorized($message = null)
    {
        if (is_null($message))
            $message = self::MESSAGE_NOT_AUTHORIZED;
        $response = [
            'success' => false,
            'message' => $message,
            'code' => 401
        ];
        return response()->json($response, 401);
    }

    public function sendAlreadyExists($message = null)
    {
        if (is_null($message))
            $message = self::MESSAGE_MUST_BE_UNIQUE;
        $response = [
            'success' => false,
            'message' => $message,
            'code' => 406
        ];
        return response()->json($response, 406);
    }

    public function sendDoesNotExist($message = null)
    {
        if (is_null($message))
            $message = self::MESSAGE_DOES_NOT_EXIST;
        $response = [
            'success' => false,
            'message' => $message,
            'code' => 406
        ];
        return response()->json($response, 406);
    }

    protected function AuthorizeAdmin($permission = null)
    {
        if (!$this->logged_user || $this->logged_user->user_type != 'admin') {
            throw new Exception("NOT AUTHORIZED FOR THIS REQUEST", 401);
        }
    }

    protected function AuthorizeSuperAdmin($permission = null)
    {
        if (!$this->logged_user || $this->logged_user->user_type != 'super_admin') {
            throw new Exception("NOT AUTHORIZED FOR THIS REQUEST", 401);
        }
    }

    protected function AuthorizeUser($permission = null)
    {
        if (!$this->logged_user || $this->logged_user->user_type != 'user') {
            throw new Exception("NOT AUTHORIZED FOR THIS REQUEST", 401);
        }
    }

    protected function userJsonResponse()
    {
        if($this->logged_user){
            return json_encode ([
                'full_name' => $this->logged_user->full_name,
                'email' => $this->logged_user->email,
                'phone' => $this->logged_user->phone,
                'account_id' => $this->logged_user->account_id,
                'account_user_uuid' => $this->logged_user->account_user_uuid,
            ]);
        }

        return null;
    }

}
