## Yojana common trait for multiple project.

## Require Packages

```
    composer require webpatser/laravel-uuid niklasravnsborg/laravel-pdf maatwebsite/excel guzzlehttp/guzzle pearl/lumen-request-validate
```

## Installation

```
    composer require yojana/base
```

## Usages

## Inject Contoller

```php

use Yojana\Http\Controllers\Controller;

```

## Inject Repositories

```php

use Yojana\Http\Repositories\YojanaBaseInterface;
use Yojana\Http\Repositories\YojanaBaseRepository;

```

## Inject Requests

```php

use Yojana\Http\Requests\BaseFormRequest;

```

## Inject Middleware

```php

// app/bootstrap.php

$app->routeMiddleware([
    ...,
    'client' => Yojana\Http\Middleware\CheckValidTokenMiddleware::class
])


```

## Inject Traits

```php

//SINGLE IMPORT
use Yojana\Traits\CommonTrait;

// OR MULTIPLE IMPORT, PHP 7+ code

use Yojana\Traits\{CommonTrait, FileUploadTrait,HelperTrait,ImageUploadTrait,RepoTrait,SecurityTrait}

```
